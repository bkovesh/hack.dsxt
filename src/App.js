/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "[a-z]" }]*/
import React, { Component } from 'react';
import { Router, browserHistory, Route, Link } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import {
  Button,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  Label,
  Menu,
  Segment,
  Step,
  Table,
} from 'semantic-ui-react'
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';
import axios from 'axios';
//
import ExchangePage from './pages/ExchangePage'
import CabinetPage from './pages/CabinetPage'
import style from './styles/allStyles'
import MainMenu from './components/MainMenu'
import Offer from './components/Offer'
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
export default class App extends Component {
  //
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  //
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' component={ExchangePage}/>
        <Route path='/cabinet' component={CabinetPage}/>
      </Router>
    );
  }
}
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/*
  // var limit = 30
  // var {origin, destination} = this.state
  // var url = `http://api.travelpayouts.com/v2/prices/latest?currency=rub&period_type=year&page=1&limit=${limit}&origin=${origin}&destination=${destination}&show_to_affiliates=true&sorting=price&token=7c62a4d56608a80f7640e0ef6d544835`
  // var res = await axios.get(url)
  // this.setState({allTickets:res.data.data})
  // console.log(res.data.data);



*/
