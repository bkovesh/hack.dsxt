/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "[a-z]" }]*/
import React, { Component } from 'react';
import { Router, browserHistory, Route, Link } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import {
  Button,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  List,
  Label,
  Menu,
  Segment,
  Step,
  Table,
} from 'semantic-ui-react'
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';
import axios from 'axios';
import style from '../styles/allStyles'
import pairs from '../data/pairs'



class ListPairs extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  //
  render() {
    const {
      pairs
    } = this.state


    const tableContent = []
    for (var key in pairs) {
      tableContent.push(
        <Table.Row key={key}>
          <Table.Cell>{key}</Table.Cell>
          <Table.Cell>{pairs[key].buyPrice}</Table.Cell>
          <Table.Cell>{pairs[key].sellPrice}</Table.Cell>
          <Table.Cell>{pairs[key].change}</Table.Cell>
        </Table.Row>
      )
    }

    return (
      <Table unstackable fixed selectable>

        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Pair</Table.HeaderCell>
            <Table.HeaderCell>Buy price</Table.HeaderCell>
            <Table.HeaderCell>Sell price</Table.HeaderCell>
            <Table.HeaderCell>Change</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {tableContent}
        </Table.Body>

      </Table>
    )
  }

  componentWillMount = () => {
    this.setState({pairs:pairs})
  }
}


export default ListPairs
