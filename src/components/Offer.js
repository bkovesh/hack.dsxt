/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "[a-z]" }]*/
import React, { Component } from 'react';
import { Router, browserHistory, Route, Link } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import {
  Button,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  Label,
  Menu,
  Segment,
  Step,
  Table,
} from 'semantic-ui-react'
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';
import axios from 'axios';
import style from '../styles/allStyles'



class Offer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allTickets: [],
      origin: 'LED',
      destination: 'SVO',
      dateFront: '',
      dateBack: '',
      time: '',
      dateTime: '',
      datesRange: '',
      passengers: 1,
    };
  }
  //
  render() {
    const {
      value,
      origin,
      destination,
      depart_date,
      return_date,
      duration,
      gate,
    } = this.props.data

    return (
      <div>
        <Grid style={style.form}>
          <Grid.Column width={4}>
            <Segment attached='top'>
              <h4 style={{}}>{value} ₽</h4>
            </Segment>
            <Button  width={4}
              content='Купить билет'
              color='grey'
              attached
            />
            <Segment attached='bottom'>
              {gate}
            </Segment>
          </Grid.Column>
          <Grid.Column width={8}>
            <Segment>
              <div>
                <h4 style={style.h4}>{origin} -> {destination}</h4><br/>
                {depart_date} <br/>
                {(duration-duration%60)/60}ч {duration%60}мин
              </div>
              <div>
                <h4 style={style.h4}>{destination} -> {origin}</h4><br/>
                {return_date} <br/>
                {(duration-duration%60)/60}ч {duration%60}мин
              </div>
            </Segment>
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}


export default Offer
