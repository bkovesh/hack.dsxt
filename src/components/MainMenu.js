/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "[a-z]" }]*/
import React, { Component } from 'react';
import { Router, browserHistory, Route, Link } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import {
  Button,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  Label,
  Menu,
  Segment,
  Step,
  Table,
} from 'semantic-ui-react'
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';
import axios from 'axios';
import style from '../styles/allStyles'

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  //
  render() {
    const { activeItem } = this.state

    return (
      <Menu unstackable="true">
        <Menu.Item>
          <img src='https://static.tildacdn.com/tild3037-6531-4436-a337-616534373064/dsxt-logo-small.svg' alt="Logo" style={{width: '6em'}}/>
        </Menu.Item>

        <Menu.Item
          name='features'
          active={activeItem === 'features'}
          to="/"
          onClick={this.handleItemClick}
        >
          Home
        </Menu.Item>

        <Menu.Item
          name='testimonials'
          active={activeItem === 'testimonials'}
          onClick={this.handleItemClick}
          to="/cabinet"
        >
          Cabinet
        </Menu.Item>

      </Menu>
    )
  }
  handleItemClick = (e, { name, to }) => {
    browserHistory.push(to)
    this.setState({ activeItem: name })
  }
}

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
export default MainMenu
