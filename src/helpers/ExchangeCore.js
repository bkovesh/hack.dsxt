const clients = {
  'address': {
    balances: []
  }
}


const pairs = {
  'ETH/BTC': {
    buyPrice: 0.02,
    sellPrice: 0.01,
    priceChange: 1,
  },
  'ETH/RUB': {
    buyPrice: 0.02,
    sellPrice: 0.01,
    priceChange: 1,
  },
};



const orders = {
  'ETH/BTC': [
    {
      id: 1,
      status: 'active', // active/processing/done/cancelling/cancelled
      owner: 'Alice', // address
      pair: 'ETH/BTC',
      type: 'buy', // buy/sell
      price: 1,
      volume: 100,
      timeOpened: 1,
      timeClosed: 2,
    },
  ]
}



const transactions = {
  'ETH/BTC': [
    {
      id: 1,
      status: 'pending', // pending/done
      owner: 'Alice',
      pair: 'ETH/BTC',
      type: 'buy', // buy/sell
      price: 1,
      volume: 100,
      time: 1,
    },
  ]
}




class ExchangeCore {
  constructor(){
    this.pairs = {}
    this.orders = {}
    this.transactions = {}
    this.newOrder = {}
  }




  // единственная внешняя функция
  // приходит ордер и его нужно добавить в список
  checkNewOrder(order) {
    if (this.isSameOrderInList(order)) {
      this.convert()
    } else {
      this.addOrderToList(order)
    }
  }




  isSameOrderInList(order) {
    let res = {};
    this.orders[order.pair].map((item, i) => {
      // если есть соответствие цена*объем
      let volumeOrder = order.volume * order.price;
      let volumeExist = item.volume / item.price;

      if (order.type == 'buy') {
        if (order.price >= item.price) {
          if (volumeExist > volumeOrder) {
            res = {'calculate': 'orders'}
          } else {
            res = {}
          }
        } else {
          res = {}
        }
      } else {
        if (order.price <= item.price) {
          if (volumeExist < volumeOrder) { // ????
            res = {'calculate': 'orders'}
          } else {
            res = {}
          }
        } else {
          res = {}
        }
      }

    })
    return res
  }




  addOrderToList(order) {
    this.orders[order.pair].map((item, i) => {
      // добавление в список только по убыванию цены*объем
      let volumeOrder = order.volume * order.price;
      let volumeExist = item.volume / item.price;
      volumeExist > volumeOrder ? 'addToListBefore&break' : 'next';
    })
  }



  convert(volume, pair, price) {
    const tx = {}
    // add transaction to list
    this.transactions.push(tx)
    // delete orders
    this.tick()
  }



  // функция информирует о сделках
  // функция вызывается при выставлении ордера, но только после того, как ордер выполняется
  tick() {

  }



}