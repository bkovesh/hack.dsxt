
var style = {
    container: {
        backgroundColor: '#F5FCFF',
    },
    titleWrapper: {

    },
    inputWrapper: {

    },
    contentContainer: {
        flex: 1 // pushes the footer to the end of the screen
    },
    header: {
      backgroundColor: '#f4e9d8',
      position: 'absolute',
      height: 90,
      width: '100%',
      top: 0
    },
    body: {
      backgroundColor: 'white',
      position: 'absolute',
      width: '100%',
      top: 90
    },
    footer: {
      backgroundColor: '#f4e9d8',
      position: 'absolute',
      height: 90,
      width: '100%',
      bottom: 0
    },
    h1: {
      marginTop: '2em',
    },
    h2: {
      marginTop: '1em',
      padding: '1em 0em 1em',
    },
    h3: {
      marginTop: '1em',
      padding: '0.5em 0em 0.5em',
    },
    h4: {
      margin: '0em 0em 0em 0em',
      padding: '1em 0em 0em 0em',
    },
    last: {
      marginBottom: '300px',
    },
    form: {
      justifyContent: 'center',
    },
    segment: {
      backgroundColor: '#f2f2f2',
      marginLeft: '0.5em',
      marginRight: '0.5em'
    }
};



export default style
