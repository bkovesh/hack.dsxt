

const orders = {
  'ETH/BTC': [
    {
      id: 1,
      status: 'active', // active/processing/done/cancelling/cancelled
      owner: 'Alice',
      pair: 'ETH/BTC',
      type: 'buy', // buy/sell
      price: 1,
      amount: 100,
      timeOpened: 1,
      timeClosed: 2,
    },
    {
      id: 2,
      status: 'active',
      owner: 'Bob',
      pair: 'ETH/BTC',
      type: 'sell',
      price: 1,
      amount: 100,
      timeOpened: 1,
      timeClosed: 2,
    },
  ]
}


export default orders