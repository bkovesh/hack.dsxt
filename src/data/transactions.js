



const transactions = {
  'ETH/BTC': [
    {
      id: 1,
      status: 'pending', // pending/done
      owner: 'Alice',
      pair: 'ETH/BTC',
      type: 'buy', // buy/sell
      price: 1,
      amount: 100,
      time: 1,
    },
  ]
}



export default transactions;