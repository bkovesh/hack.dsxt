/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "[a-z]" }]*/
import React, { Component } from 'react';
import { Router, browserHistory, Route, Link } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import {
  Button,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  Label,
  Menu,
  Segment,
  Step,
  Table,
} from 'semantic-ui-react'
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';
import axios from 'axios';
import style from '../styles/allStyles'
import MainMenu from '../components/MainMenu'
import Offer from '../components/Offer'
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
class CabinetPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allTickets: [],
      origin: 'LED',
      destination: 'SVO',
      dateFront: '',
      dateBack: '',
      time: '',
      dateTime: '',
      datesRange: '',
      passengers: 1,
    };
    this.getPrices = this.getPrices.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  //
  render() {
    const {
      origin,
      destination,
      dateBack,
      dateFront,
      passengers,
    } = this.state

    return (
      <div className="App-body" >
        <MainMenu/>

        <Header as='h2' content='Cabinet' style={style.h2} textAlign='center' />

        <Header as='h3' content='Balances' style={style.h3} textAlign='center' />
        <Segment style={style.segment}>
          Balances
        </Segment>

        <Header as='h3' content='Opened orders' style={style.h3} textAlign='center' />
        <Segment style={style.segment}>
          Opened orders
        </Segment>

        <Header as='h3' content='Transaction history' style={style.h3} textAlign='center' />
        <Segment style={style.segment}>
          Transaction history
        </Segment>

      </div>
    );
  }


  getPrices = async () => {
    //http://localhost:3000/купить_билет/из_LED_в_SVO_12-06-2019_24-06-2019_1_взрослый_бизнес-класс
    var limit = 20
    var {origin, destination, dateFront, dateBack, passengers} = this.state
    var link = `/из_${origin}_в_${destination}_${dateFront}_${dateBack}_${passengers}_взрослый_бизнес-класс`
    fetch('/api'+encodeURI('/купить_билет'+link))
      .then(res => res.json())
      .then(users => this.setState({allTickets:users}));
  }


  getAllTickets = (t) => {
    var res = []
    t.map((item,i) => {
      var s = ''
      for (var key in item) {
        s += item[key] + ' '
      }
      res.push(
        <div key={i}>
          <Offer
            data={item}
          />
        </div>
      )
      return res
    })
    return res
  }


  handleChange = (e,n) => {
    var o = {}
    o[n] = e.target.value
    this.setState(o);
  }



  componentWillMount = () => {
    // this.getPrices()
  }
}
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
export default CabinetPage
