/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "[a-z]" }]*/
import React, { Component } from 'react';
import { Router, browserHistory, Route, Link } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import {
  Button,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  Label,
  Menu,
  Segment,
  Step,
  Table,
} from 'semantic-ui-react'
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';
import axios from 'axios';
import style from '../styles/allStyles'
import MainMenu from '../components/MainMenu'
import Offer from '../components/Offer'
import ListPairs from '../components/ListPairs'

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
class ExchangePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      choosenItem1: 'ETH',
      amount1: 0,
      price1: 0,
      comission1: 0,
      sum1: 0,
      choosenItem2: 'BTC',
      amount2: 0,
      price2: 0,
      comission2: 0,
      sum2: 0,
    };
    this.getPrices = this.getPrices.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  //
  render() {
    const {
      choosenItem1,
      amount1,
      price1,
      comission1,
      sum1,
      choosenItem2,
      amount2,
      price2,
      comission2,
      sum2,
    } = this.state;

    return (
      <div className="App-body" >
        <MainMenu/>

        <Header as='h2' content='Exchange' style={style.h2} textAlign='center' />

        <Grid columns='equal' celled='internally' stackable style={style.form}>
          <Grid.Column width={10}>

            <Grid.Row>
              <Header as='h3' content='Pairs list' style={style.h3} textAlign='center' />
              <Segment textAlign='center' style={style.segment}>
                <ListPairs/>
              </Segment>
            </Grid.Row>

            <Grid.Row>
              <Header as='h3' content='Chart' style={style.h3} textAlign='center' />
              <Segment textAlign='center' style={style.segment}>
                Chart
              </Segment>
            </Grid.Row>

            <Grid.Row>
              <Header as='h3' content='Buy orders' style={style.h3} textAlign='center' />
              <Segment textAlign='center' style={style.segment}>
                Buy orders
              </Segment>
            </Grid.Row>

            <Grid.Row>
              <Header as='h3' content='Sell orders' style={style.h3} textAlign='center' />
              <Segment textAlign='center' style={style.segment}>
                Sell orders
              </Segment>
            </Grid.Row>

            <Grid.Row>
              <Header as='h3' content='Trade history' style={style.h3} textAlign='center' />
              <Segment textAlign='center' style={style.segment}>
                Trade history
              </Segment>
            </Grid.Row>

          </Grid.Column>
          <Grid.Column>

            <Grid.Row  unstackable="true">
              <Header as='h3' content='Trading' style={style.h3} textAlign='center' />
              <Segment style={style.segment}>
                <Form unstackable style={{}}>
                  <Form.Group style={style.form}>
                    <Form.Field width={4} style={{color: 'teal'}}>
                      <label>Amount of {choosenItem1}</label>
                      <input
                        name='amount1'
                        value={amount1}
                        onChange={(e) => this.handleChange(e,'amount1')}
                      />
                    </Form.Field>
                    <Form.Field width={4} style={{color: 'teal'}}>
                      <label>Amount of {choosenItem2}</label>
                      <input
                        name='amount2'
                        value={amount2}
                        onChange={(e) => this.handleChange(e,'amount2')}
                      />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group style={style.form}>
                    <Form.Field width={4} >
                      <label>Price of {choosenItem1}</label>
                      <input
                        name='price1'
                        value={price1}
                        placeholder='0'
                        onChange={(e) => this.handleChange(e,'price1')}
                      />
                    </Form.Field>
                    <Form.Field width={4} >
                      <label>Price of {choosenItem2}</label>
                      <input
                        name='price2'
                        value={price2}
                        placeholder='0'
                        onChange={(e) => this.handleChange(e,'price2')}
                      />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group style={style.form}>
                    <Form.Field width={4} >
                      <label>Sum of {choosenItem2} to pay</label>
                      <input
                        name='sum1'
                        value={sum1}
                        placeholder='0'
                        onChange={(e) => this.handleChange(e,'sum1')}
                      />
                    </Form.Field>
                    <Form.Field width={4} >
                      <label>Sum of {choosenItem1} to pay</label>
                      <input
                        name='sum2'
                        value={sum2}
                        placeholder='0'
                        onChange={(e) => this.handleChange(e,'sum2')}
                      />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group style={style.form}>
                    <Form.Field width={4} >
                      <label>Comission</label>
                      {comission1} {choosenItem1}
                    </Form.Field>
                    <Form.Field width={4} >
                      <label>Comission</label>
                      {comission2} {choosenItem2}
                    </Form.Field>
                  </Form.Group>
                  <Form.Group style={style.form}>
                    <Form.Field width={4} style={{textAlign: 'center'}}>
                      <Button fluid
                        content={`Buy ${choosenItem1}`}
                        color='grey'
                        onClick={this.getPrices}
                      />
                    </Form.Field>
                    <Form.Field width={4} style={{textAlign: 'center'}}>
                      <Button fluid
                        content='Buy BTC'
                        color='grey'
                        onClick={this.getPrices}
                      />
                    </Form.Field>
                  </Form.Group>
                </Form>
              </Segment>
            </Grid.Row>

            <Grid.Row>
              <Header as='h3' content='My orders' style={style.h3} textAlign='center' />
              <Segment textAlign='center' style={style.segment}>
                My orders
              </Segment>
            </Grid.Row>

          </Grid.Column>
        </Grid>

      </div>
    );
  }


  getPrices = async () => {
    //http://localhost:3000/купить_билет/из_LED_в_SVO_12-06-2019_24-06-2019_1_взрослый_бизнес-класс
    var limit = 20
    var {origin, destination, dateFront, dateBack, passengers} = this.state
    var link = `/из_${origin}_в_${destination}_${dateFront}_${dateBack}_${passengers}_взрослый_бизнес-класс`
    fetch('/api'+encodeURI('/купить_билет'+link))
      .then(res => res.json())
      .then(users => this.setState({allTickets:users}));
  }


  getAllTickets = (t) => {
    var res = []
    t.map((item,i) => {
      var s = ''
      for (var key in item) {
        s += item[key] + ' '
      }
      res.push(
        <div key={i}>
          <Offer
            data={item}
          />
        </div>
      )
      return res
    })
    return res
  }


  handleChange = (e,n) => {
    var o = {}
    o[n] = e.target.value
    this.setState(o);
  }


  handleChangeCalendar = (event, {name, value}) => {
    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: value });
    }
  }


  componentWillMount = () => {
    // this.getPrices()
  }
}
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
export default ExchangePage
